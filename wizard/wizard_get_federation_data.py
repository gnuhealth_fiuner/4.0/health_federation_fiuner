from trytond.pyson import Equal, Eval, Not
from trytond.model import ModelView, fields
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateAction, StateTransition, \
    Button

from datetime import datetime,timedelta


class GetFederationDataStart(ModelView):
    'Get Federation Data - Start'
    __name__ = 'gnuhealth.federation.get_federation_data.start'

    name = fields.Char('Name')
    lastname = fields.Char('Lastname')
    puid = fields.Char('PUID', required=True)
    federation_account = fields.Char('Federation account', readonly=True)
    gender = fields.Selection([
        ('m','Masculine'),
        ('f','Femenine'),
        ],'Gender', required=True)

    @fields.depends('puid')
    def on_change_with_federation_account(self, name=None):
        pool = Pool()
        Patient = pool.get('gnuhealth.patient')
        res = ''
        if self.puid:
            patients = Patient.search([('puid','=',self.puid)])
            if patients:
                res = patients[0].name.federation_account
        return res

class GetFederationDataPatientFound(ModelView):
    'Get Federation Data - Patient Found'
    __name__ = 'gnuhealth.federation.get_federation_data.patient_found'

    name = fields.Char('Name', readonly=True)
    lastname = fields.Char('Lastname', readonly=True)
    puid = fields.Char('PUID', readonly=True) #TODO add to people object
    gender = fields.Selection([
        ('m','Masculine'),
        ('f','Femenine'),
        ],'Gender', readonly=True)
    federation_account = fields.Char('Federation account',readonly=True)
    get_data = fields.Boolean('Show data')


class GetFederationDataPatientFoundList(ModelView):
    'Get Federation Data - Patient List Found'
    __name__ = 'gnuhealth.federation.get_federation_data.patient_list_found'

    patient_list = fields.One2Many(
        'gnuhealth.federation.get_federation_data.patient_found', None, 'Patient Found')


class GetFederationDataPols(ModelView):
    'Get Federation Data - Evaluation Found'
    __name__ = 'gnuhealth.federation.get_federation_data.pols'

    page_type = fields.Selection([
        (None, ''),
        ('biographical', 'Biographical'),
        ('medical', 'Medical'),
        ('demographical', 'Demographical'),
        ('social', 'Social'),
        ], 'Page type', sort=False, readonly=True)
    medical_context = fields.Selection([
        (None, ''),
        ('health_condition', 'Health Condition'),
        ('encounter', 'Encounter'),
        ('procedure', 'Procedure'),
        ('immunization','Immunization'),
        ('prescription', 'Prescription'),
        ('surgery', 'Surgery'),
        ('hospitalization', 'Hospitalization'),
        ('lab', 'lab'),
        ('dx_imaging', 'Dx Imaging'),
        ('genetics', 'Genetics'),
        ('family', 'Family history'),
        ('birth', 'Birth'),
        ('death', 'Death'),
        ], 'Medical Context', sort=False,
        states={'invisible': Not(Equal(Eval('page_type'), 'medical'))
            },
        readonly=True)
    pol = fields.Text('Page of Life Information')


class GetFederationDataPolsList(ModelView):
    'Get Federation Data - Evaluation List Found'
    __name__ = 'gnuhealth.federation.get_federation_data.pols_list'

    pols_list = fields.One2Many(
        'gnuhealth.federation.get_federation_data.pols',None,'Pols list',
        readonly=True)


class GetFederationDataWizard(Wizard):
    'Get Federation Data - Wizard'
    __name__ = 'gnuhealth.federation.get_federation_data.wizard'

    @classmethod
    def __setup__(cls):
        super(GetFederationDataWizard,cls).__setup__()

    start_state = 'start'
    start = StateView('gnuhealth.federation.get_federation_data.start',
        'health_federation_fiuner.get_federation_data_start_form_view', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Look for this patient', 'check_data', 'tryton-ok'),
            ])

    check_data = StateTransition()

    found_list = StateView('gnuhealth.federation.get_federation_data.patient_list_found',
        'health_federation_fiuner.get_federation_data_patient_list_found_form_view',[
            Button('Cancel','end','tryton-cancel'),
            Button('Get Federation POL Data','look_for_pol','tryton-ok'),
            ])

    look_for_pol = StateTransition()

    pols_list = StateView('gnuhealth.federation.get_federation_data.pols_list',
        'health_federation_fiuner.get_federation_data_pols_list_form_view',[
            Button('Cancel','end','tryton-cancel'),
            Button('Look for another patient','start','tryton-ok'),
            Button('Look again','look_for_pol','tryton-ok'),
            ])

    def transition_check_data(self):
        import requests
        pool = Pool()
        Federation = pool.get('gnuhealth.federation.config')(1)

        if not (Federation and Federation.enabled and Federation.host and\
            Federation.port and Federation.user and Federation.password):
            raise UserError('no_fed_config')
        ssl = 'https://' if Federation.ssl else 'http://'
        host = Federation.host
        port = Federation.port and str(Federation.port) or ''
        fed_user = Federation.user
        pw = Federation.password
        url = ssl+host+':'+port+'/people'
        auth = (fed_user, pw)

        name = self.start.name
        lastname = self.start.lastname
        puid = self.start.puid
        federation_account = self.start.federation_account
        gender = self.start.gender
        url_fed = url
        if federation_account:
            url_fed = url+'/'+federation_account
        else:
            raise UserError('no_federation_account_deep_search') #TODO set to warning
        person = requests.get(url_fed, auth=auth, verify=False)
        if url_fed and person.raise_for_status():
            raise UserError('connection_issue')
        self.check_data.people = [person.json()]

        people = requests.get(url, auth=auth, verify=False)
        if people.raise_for_status():
            raise UserError('connection_issue')

        people = [d[0] for d in person.json()\
            if ('lastname' in d[0] and d[0]['lastname'].lower() == lastname.lower() and\
               'name' in d[0] and d[0]['name'].lower() == name.lower()) or
                'puid' in d[0] and d[0]['puid'] == puid]

        if people:
            self.check_data.people +=people.json()
        return 'found_list'

    def default_found_list(self,fields):
        people = self.check_data.people
        res = []
        for person in people:
            res.append({
                'name': person['name'],
                'lastname': person['lastname'],
                'gender': person['gender'],
                #'puid': person['puid'],
                'federation_account': person['id'],
                'get_data': True if len(people) ==1 else False
                })
        return {'patient_list': res}

    def transition_look_for_pol(self):
        import requests
        pool = Pool()
        Federation = pool.get('gnuhealth.federation.config')(1)
        people = [x.federation_account for x in self.found_list.patient_list if x.get_data == True]

        ssl = 'https://' if Federation.ssl else 'http://'
        host = Federation.host
        port = Federation.port and str(Federation.port) or ''
        fed_user = Federation.user
        pw = Federation.password
        url = ssl+host+':'+port+'/pols'
        auth = (fed_user, pw)

        pols = []
        for person in people:
            url_fed = url+'/'+person
            pol = requests.get(url_fed, auth=auth, verify=False)
            if url_fed and pol.raise_for_status():
                raise UserError('connection_issue')
            pols += pol.json()
        self.look_for_pol.pols = pols
        return 'pols_list'

    def default_pols_list(self,fields):
        def plain_to_richtext(mystring):
            return mystring.replace('\n','</div><div>')
        def datetime2local(mystring):
            pol_date = datetime.strptime(mystring.replace('T',' '), '%Y-%m-%d %H:%M:%S') - timedelta(hours=3)
            return pol_date.strftime("%d/%m/%Y, %H:%M:%S")

        pols = [x[0] for x in self.look_for_pol.pols]
        res = []

        #append medical/encounters
        if next((x for x in pols\
            if x['page_type']=='medical'\
                and x['medical_context']=='encounter'),None):
            res.append({
                'page_type': 'medical',
                'medical_context': 'encounter',
                'pol': '<div></div>'.join(
                    f"<div align='center'><b><font size='6'> {x['node'].upper()}  {datetime2local(x['page_date'])} "+
                    f" {x['summary'].upper()} </font></b></div>"+
                    f"<div> {plain_to_richtext(x['info'])} </div>"+
                    f"<div align='right'><font size='5'><i><b>{plain_to_richtext(x['author'])}   </b></i></font></div>"
                    for x in pols if 'page_type' in x and x['page_type']=='medical' and\
                        'medical_context' in x and x['medical_context']=='encounter')
                })
        #append appointments
        #if next((x for x in pols\
            #if x['page_type']=='medical'\
                #and x['medical_context']=='encounter'),None):
            #res.append({})
            #res.append({
                #'page_type': 'medical',
                #'medical_context': 'encounter',
                #'pol': '<div></div>'.join(
                    #f"<div align='center'><b><font size='6'> {x['node'].upper()}  {datetime2local(x['page_date'])} "+
                    #f" {x['summary'].upper()} </font></b></div>"+
                    #f"<div> {plain_to_richtext(x['info'])} </div>"+
                    #f"<div align='right'><font size='5'><i><b>{plain_to_richtext(x['author'])}   </b></i></font></div>"
                    #for x in pols if 'page_type' in x and x['page_type']=='medical' and\
                        #'medical_context' in x and x['medical_context']=='encounter')
                #})
        return {
            'pols_list': res
            }
